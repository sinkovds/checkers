/**
 * Created by dmitrysinkov on 02.02.15.
 */
GameApp.factory('Player',['Checker', function Player(Checker) {
    function Player() {
        this.checkers = [];
        this.checkers_map = null;
        this.current_checker = null;
    };

    Player.prototype.checkers = [];
    Player.prototype.name = null;
    Player.prototype.checkers_map = null;
    Player.prototype.current_checker = null;

    Player.prototype.set_name = function($name){
        this.name = $name;
    }

    Player.prototype.set_checkers_map = function($checkers_map){
        this.checkers_map = $checkers_map;

        for(var i in this.checkers_map){
            for(var j in this.checkers_map[i]){

                if(this.checkers_map[i][j] == 1)
                {
                    var checker  = new Checker;
                    checker.player = this;
                    checker.x = j;
                    checker.y = i;
                    this.checkers.push(checker);
                }
            }
        }
    }


    Player.prototype.get_by_coordinates = function (x, y){
        for(var i = 0; i< this.checkers.length;i++)
        {
            if(this.checkers[i].x == x && this.checkers[i].y == y)
                return this.checkers[i];
        }

        return null;
    };

    Player.prototype.delete_by_coordinates = function (x, y){
        for(var i = 0; i< this.checkers.length;i++)
        {
            if(this.checkers[i].x == x && this.checkers[i].y == y)
            {
                this.checkers_map[y][x] = 0 ;
                this.checkers[i].viewObj.remove();
                this.checkers.splice(i, 1);
                return;
            }
        }
    };

    Player.prototype.set_type_by_coordinates = function (x, y, type){
        for(var i = 0; i< this.checkers.length;i++)
        {
            if(this.checkers[i].x == x && this.checkers[i].y == y)
            {
                this.checkers[i].set_type(type);
                return this.checkers[i];
            }
        }

    };

    return Player;
}]);