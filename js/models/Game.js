/**
 * Created by dmitrysinkov on 02.02.15.
 */

GameApp.factory('Game', ['$rootScope', 'Player','Desk', function Game($rootScope, Player, Desk) {
    function Game() {

    };

    Game.prototype.scope = null;
    Game.current_player = null;
    Game.prototype.all_time = 0;
    Game.prototype.timer1 = 0;
    Game.prototype.timer2 = 0;
    Game.prototype.players = [];
    Game.prototype.desk = null;
    Game.prototype.name = "";
    Game.prototype.cell_width = 50;
    Game.prototype.more_step = false;
    Game.prototype.remove_checker = false;

    Game.prototype.score = [0, 0];
    Game.prototype.globalscore = [0, 0];

    Game.prototype.timer = null;

    Game.prototype.add_player = function($player){
        this.players.push($player);
    }

    Game.prototype.set_current_player = function($player){
        this.current_player = $player;
    }

    Game.prototype.set_desk = function($desk){
        this.desk = $desk;
    }

    Game.prototype.set_name = function($name){
        this.name = $name;
    }

    Game.prototype.check_more_step = function(x1, y1, player, checker){

        x1 = parseInt(x1);
        y1 = parseInt(y1);

        //если не дамка
        if(checker.type == 0){
            var points = [ [x1+2,y1+2], [x1-2,y1+2], [x1+2,y1-2], [x1-2,y1-2] ];
            for(i in points){
                var x3 = (x1 + points[i][0])/2;
                var y3 = (y1 + points[i][1])/2;
                var second_player = player == 1 ? 0 : 1;
                var second_checker = this.players[second_player].get_by_coordinates(x3,y3);
                if(second_checker != null && this.check_cell(points[i][0],points[i][1]) ){
                    return true;
                }
            }
        }
        if(checker.type == 1){


            for(var i = 1; i<= (this.desk.map.length - x1); i++){
                if(this.get_count_ch(x1,y1, x1 + i , y1 +i, player, checker) === true)
                    return true;
            }
            for(var i = 1; i<= (this.desk.map.length - x1); i++){
                if(this.get_count_ch(x1,y1, x1 + i , y1 - i, player, checker) === true)
                    return true;
            }
            for(var i = 1; i<= x1; i++){
                if(this.get_count_ch(x1,y1, x1 - i , y1 +i, player, checker) === true){

                    return true;
                }

            }
            for(var i = 1; i<= x1; i++){
                if(this.get_count_ch(x1,y1, x1 - i , y1 - i, player, checker) === true)
                    return true;
            }
        }

        return false;
    }

    Game.prototype.get_count_ch = function (x1, y1, x2, y2, player, checker) {

        var checkers_count = 0;
        var checkers_ = null;

        x1 = parseInt(x1);
        y1 = parseInt(y1);
        x2 = parseInt(x2);
        y2 = parseInt(y2);


        if(y2 > y1){
            if(x2 > x1){
                for(var i = 1 ;i<= Math.abs(y2-y1); i++){
                    for(var players in this.players){
                        checkers_ = this.players[players].get_by_coordinates(x1+i,y1+i);
                        if(checkers_ != null && this.check_cell(x2,y2))
                        {
                            if(checkers_.player == this.players[this.current_player])
                                return false;

                            checkers_count++;


                            if(checkers_count>1)
                                return false;
                        }
                    }
                }
            }else
            {
                for(var i = 1 ;i<= Math.abs(y2-y1); i++){
                    for(var players in this.players){
                        checkers_ = this.players[players].get_by_coordinates(x1 - i,y1 + i)
                        if(checkers_ != null && this.check_cell(x2,y2))
                        {

                            if(checkers_.player == this.players[this.current_player])
                                return false;

                            checkers_count++;
                            if(checkers_count>1)
                                return false;

                        }
                    }

                }
            }
        }else
        {
            if(x2>x1){
                for(var i = 1 ;i<= Math.abs(y2-y1); i++){
                    for(var players in this.players) {
                        checkers_  = this.players[players].get_by_coordinates(x1 + i, y1 - i);
                        if (checkers_ != null && this.check_cell(x2,y2)) {
                            if(checkers_.player == this.players[this.current_player])
                                return false;

                            checkers_count++;
                            if(checkers_count>1)
                                return false;
                        }
                    }
                }
            }else
            {

                for(var i = 1 ;i<= Math.abs(y2-y1); i++){
                    for(var players in this.players){
                        checkers_  = this.players[players].get_by_coordinates(x1-i,y1-i);
                        if( checkers_ != null && this.check_cell(x2,y2))
                        {
                            if(checkers_.player == this.players[this.current_player])
                                return false;

                            checkers_count++;
                            if(checkers_count>1)
                                return false;

                        }
                    }

                }
            }
        }


        if(checkers_count == 1)
            return true;

        return false;
    }



    Game.prototype.allow_step = function(x1, y1, x2, y2, player, checker, check_only){


        x1 = parseInt(x1);
        y1 = parseInt(y1);
        x2 = parseInt(x2);
        y2 = parseInt(y2);

        if(!this.check_cell(x2,y2))
            return false;



        var second_player = player == 1 ? 0 : 1;



        //если не дамка
        if(checker.type == 0){

            //проверка на находящиеся рядом шашки противника


            var x3 = (x2+x1)/2;
            var y3 = (y2+y1)/2;
            var second_checker = this.players[second_player].get_by_coordinates(x3,y3);

            if(!check_only && second_checker != null && (Math.abs(x1-x2) == 2 || Math.abs(y1-y2) == 2) && this.check_cell(x2,y2) ){
                this.players[second_player].delete_by_coordinates(x3,y3);
                this.remove_checker = true;
                this.more_step = this.check_more_step(x2,y2,this.current_player,this.players[this.current_player].current_checker);
                this.score[this.current_player]++;
                this.scope.updateInfo();
                return true;
            }


            if((Math.abs(x1-x2) > 1 || Math.abs(y1-y2) > 1) && !check_only)
                return false;

            if(player == 0){

                if(y1 - y2  <= 0)
                    return false;
            }else
            {
                if(y1 - y2 >= 0)
                    return false;
            }

        }
        if(checker.type == 1){

            // проверка что дамка ходит по диагонали
            if(!(y2 == x2 - x1 + y1 || y2 == -x2 + x1 + y1))
                return false;

            if(y2 == y1)
                return false;

            var checkers_count = 0;
            var checkers_ = null;
            var checkers_buf = null;

            if(y2 > y1){
                if(x2 > x1){
                    for(var i = 1 ;i< Math.abs(y2-y1); i++){
                        for(var players in this.players){
                            checkers_ = this.players[players].get_by_coordinates(x1+i,y1+i);
                            if(checkers_ != null && this.check_cell(x2,y2))
                            {
                                if(checkers_.player == this.players[this.current_player])
                                    return false;

                                checkers_count++;
                                if(checkers_count>1)
                                    return false;

                                checkers_buf = checkers_;

                            }
                        }
                    }
                }else
                {
                    for(var i = 1 ;i< Math.abs(y2-y1); i++){
                        for(var players in this.players){
                            checkers_ = this.players[players].get_by_coordinates(x1 - i,y1 + i)
                            if(checkers_ != null && this.check_cell(x2,y2))
                            {
                                if(checkers_.player == this.players[this.current_player])
                                    return false;

                                checkers_count++;
                                if(checkers_count>1)
                                    return false;

                                checkers_buf = checkers_;

                            }
                        }

                    }
                }
            }else
            {
                if(x2>x1){
                    for(var i = 1 ;i< Math.abs(y2-y1); i++){
                        for(var players in this.players) {
                            checkers_  = this.players[players].get_by_coordinates(x1 + i, y1 - i);
                            if (checkers_ != null && this.check_cell(x2,y2)) {
                                if(checkers_.player == this.players[this.current_player])
                                    return false;

                                checkers_count++;
                                if(checkers_count>1)
                                    return false;

                                checkers_buf = checkers_;
                            }
                        }
                    }
                }else
                {
                    for(var i = 1 ;i< Math.abs(y2-y1); i++){
                        for(var players in this.players){
                            checkers_  = this.players[players].get_by_coordinates(x1-i,y1-i);
                            if( checkers_ != null && this.check_cell(x2,y2))
                            {
                                if(checkers_.player == this.players[this.current_player])
                                    return false;

                                checkers_count++;
                                if(checkers_count>1)
                                    return false;

                                checkers_buf = checkers_;

                            }
                        }

                    }
                }
            }



            if(!check_only && checkers_count == 1 && this.check_cell(x2,y2) && checkers_buf != null){

                this.players[second_player].delete_by_coordinates(checkers_buf.x,checkers_buf.y);
                this.remove_checker = true;
                this.more_step = this.check_more_step(x2,y2,this.current_player,this.players[this.current_player].current_checker);
                this.score[this.current_player]++;


                return true;
            }

        }
        return true;
    }



    Game.prototype.check_cell = function(x, y){

        x = parseInt(x);
        y = parseInt(y);

        if(this.desk.map[y] == undefined)
            return false;
        if(this.desk.map[y][x] != 1)
            return false;

        for(var i in this.players){
            if(this.players[i].checkers_map[y][x] == 1)
                return false;
        }

        return true;
    }

    Game.prototype.check_steps = function(player){


        for(var i in this.players[player].checkers){
            this.players[player].checkers[i].x = parseInt(this.players[player].checkers[i].x);
            this.players[player].checkers[i].y = parseInt(this.players[player].checkers[i].y);
            var exist = this.allow_step(this.players[player].checkers[i].x,
                    this.players[player].checkers[i].y,
                    this.players[player].checkers[i].x - 1 ,
                    this.players[player].checkers[i].y - 1,
                    player,
                    this.players[player].checkers[i],
                    true)
                || this.allow_step(this.players[player].checkers[i].x,
                    this.players[player].checkers[i].y,
                    this.players[player].checkers[i].x - 2 ,
                    this.players[player].checkers[i].y - 2,
                    player,
                    this.players[player].checkers[i],
                    true);

            if(exist)
                return true;
            exist = this.allow_step(this.players[player].checkers[i].x,
                this.players[player].checkers[i].y,
                this.players[player].checkers[i].x + 1 ,
                this.players[player].checkers[i].y - 1,
                player,
                this.players[player].checkers[i],
                true)
            || this.allow_step(this.players[player].checkers[i].x,
                this.players[player].checkers[i].y,
                this.players[player].checkers[i].x + 2 ,
                this.players[player].checkers[i].y - 2,
                player,
                this.players[player].checkers[i],
                true);

            if(exist)
                return true;



            exist = this.allow_step(this.players[player].checkers[i].x,
                this.players[player].checkers[i].y,
                this.players[player].checkers[i].x - 1 ,
                this.players[player].checkers[i].y + 1,
                player,
                this.players[player].checkers[i],
                true)
            || this.allow_step(this.players[player].checkers[i].x,
                this.players[player].checkers[i].y,
                this.players[player].checkers[i].x - 2 ,
                this.players[player].checkers[i].y + 2,
                player,
                this.players[player].checkers[i],
                true);

            if(exist)
                return true;
            exist = this.allow_step(this.players[player].checkers[i].x,
                this.players[player].checkers[i].y,
                this.players[player].checkers[i].x + 1 ,
                this.players[player].checkers[i].y + 1,
                player,
                this.players[player].checkers[i],
                true)
            || this.allow_step(this.players[player].checkers[i].x,
                this.players[player].checkers[i].y,
                this.players[player].checkers[i].x + 2 ,
                this.players[player].checkers[i].y + 2,
                player,
                this.players[player].checkers[i],
                true);


            if(exist)
                return true;
        }
        return false;
    };

    Game.prototype.has_remove = function(player){

        if(!player)
            player = this.current_player;


        for(var i in this.players[player].checkers){
            if(this.check_more_step(this.players[player].checkers[i].x,this.players[player].checkers[i].y, player,this.players[player].checkers[i]))
            {
                return true;
            }
        }
        return false;
    }

    Game.prototype.has_remove_all_players = function(){

        for(var player in this.players )
            for(var i in this.players[player].checkers){
                if(this.check_more_step(this.players[player].checkers[i].x,this.players[player].checkers[i].y, player,this.players[player].checkers[i]))
                {
                    return true;
                }
            }
        return false;
    }



    Game.prototype.set_location = function(x,y){

        x = parseInt(x);
        y = parseInt(y);

        if(this.current_player == null || this.players[this.current_player].current_checker == null)
            return;

        var x1 = parseInt(this.players[this.current_player].current_checker.x);
        var y1 = parseInt(this.players[this.current_player].current_checker.y);

        var x2 = parseInt(x);
        var y2 = parseInt(y);

        if(!this.allow_step(x1,y1,x2,y2,this.current_player,this.players[this.current_player].current_checker))
            return;


        if(!this.remove_checker && this.has_remove())
            return false;

        this.remove_checker = false;
        if(!this.check_cell(x,y))
            return;



        this.players[this.current_player].checkers_map[this.players[this.current_player].current_checker.y][this.players[this.current_player].current_checker.x] = 0;
        this.players[this.current_player].checkers_map[y][x] = 1;
        this.players[this.current_player].current_checker.x = x;
        this.players[this.current_player].current_checker.y = y;
        this.players[this.current_player].current_checker.viewObj.attr('x',x);
        this.players[this.current_player].current_checker.viewObj.attr('y',y);

        this.players[this.current_player].current_checker.viewObj.css({
            top: y * this.cell_width + 5,
            left: x * this.cell_width + 5
        });
        this.check_type_change(x, y);




        //проверка до смены хода
        var game_over = this.check_game_over();

        //проверка после смены хода, на случай если шашка зажата
        if(!this.more_step && !game_over){
            this.next_step();
            this.check_game_over();
        }

        //для клиент-серверной архитектуры
        this.send_state();

    }

    Game.prototype.send_state = function(){
        //заглушка для отправки данных об игре на сервер
    }

    Game.prototype.check_game_over = function(){

        var second_player = this.current_player == 0 ? 1 : 0;


        if(this.players[second_player].checkers.length == 0){
            clearInterval(this.timer);
            this.scope.add_score(this.current_player);
            this.scope.win_message = "Поздравляем, победил " + this.players[this.current_player].name + "!";
            this.scope.$apply();
            return true;
        }

        if(!this.check_steps(this.current_player) && this.has_remove_all_players() == false){
            clearInterval(this.timer);
            this.scope.add_score(second_player);
            this.scope.win_message = "Поздравляем, победил " + this.players[second_player].name + "!";
            this.scope.$apply();
            return true;
        }



        return false;

    }

    Game.prototype.check_type_change = function(x,y){

        x = parseInt(x);
        y = parseInt(y);

        if(this.current_player == 0){
            if(y==0){
                this.players[this.current_player].set_type_by_coordinates(x,y,1);
            }
        }else{
            if(y == this.desk.map.length-1){
                this.players[this.current_player].set_type_by_coordinates(x,y,1);
            }
        }
    }


    Game.prototype.next_step = function(){

        this.players[this.current_player].current_checker.viewObj.removeClass('checker_select');
        this.players[this.current_player].current_checker = null;

        this.current_player++;

        if(this.current_player > this.players.length -1)
        {
            this.current_player = 0;
        }
        this.scope.$apply();


    }

    Game.prototype.draw_checkers = function(){


        for(var pl_index in this.players){

            for(var i in this.desk.map){
                for(var j in this.desk.map[i]){
                    var checker = this.players[pl_index].get_by_coordinates(j,i);
                    if(checker !== null){

                        var obj = $('<div/>', {
                            'id': 'plchk_'+ pl_index + '_' + i + '_' + j,
                            'class': pl_index == 0 ? 'checker checker_white' : 'checker checker_black',
                            'x':j,
                            'y':i,
                            'player': pl_index
                        });

                        obj.css({
                            top: i * this.cell_width + 5,
                            left: j * this.cell_width + 5
                        });

                        $('<div/>', {
                            'class':'crown'
                        }).appendTo(obj);

                        checker.viewObj = obj;
                        obj.click(function(e){
                            e.preventDefault();
                            var scope = angular.element('body').scope();
                            if(scope.game.more_step)
                                return false;

                            var x = $(this).attr('x');
                            var y = $(this).attr('y');
                            var player = $(this).attr('player');
                            var checker = scope.game.players[scope.game.current_player].get_by_coordinates(x,y);
                            if(checker != null) {
                                $('.checker').removeClass('checker_select');
                                $(this).addClass('checker_select');
                                scope.game.players[scope.game.current_player].current_checker = checker;
                            }
                        });

                        obj.appendTo('#desk');
                    }
                }
            }

        }
    }

    Game.prototype.draw = function() {
        var desc = new Desk();
        this.set_desk(desc);
        this.set_name("Шашки");
        var player1 = new Player();
        player1.set_name("Новый игрок 1");

        player1.set_checkers_map(
            [
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [1, 0, 1, 0, 1, 0, 1, 0],
                [0, 1, 0, 1, 0, 1, 0, 1],
                [1, 0, 1, 0, 1, 0, 1, 0]
            ]
        );

        var player2 = new Player();
        player2.set_name("Новый игрок 2");

        player2.set_checkers_map(
            [
                [0, 1, 0, 1, 0, 1, 0, 1],
                [1, 0, 1, 0, 1, 0, 1, 0],
                [0, 1, 0, 1, 0, 1, 0, 1],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0]
            ]
        );


        this.add_player(player1);
        this.add_player(player2);



        this.set_current_player(0);
        this.draw_checkers();
    }

    Game.prototype.remove = function(){

        for(i in this.players) {
            for (j in this.players[i].checkers) {
                this.players[i].checkers[j].viewObj.remove();
                delete delete this.players[i].checkers[j];
            }
            delete this.players[i].checkers;
        }
        delete this.players;
        this.players = [];

        this.timer1 = 0;
        this.timer2 = 0;
        this.score = [0, 0];
        this.all_time = 0;
        this.more_step = false;
        this.remove_checker = false;

        clearInterval(this.timer);



    }

    Game.prototype.run = function(){



        var obj = this;
        this.timer = setInterval(function() {

            obj.all_time++;
            if (obj.current_player == 0) {
                obj.timer1++;
            } else {
                obj.timer2++;
            }


            obj.scope.$apply();
        }, 1000);
    }

    return Game;
}]);