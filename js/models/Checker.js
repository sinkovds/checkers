/**
 * Created by dmitrysinkov on 02.02.15.
 */

GameApp.factory('Checker', function Checker() {
    function Checker() {

    };

    Checker.prototype.type = 0;
    Checker.prototype.viewObj = null;
    Checker.prototype.x = 0;
    Checker.prototype.y = 0;
    Checker.prototype.player = null;

    Checker.prototype.set_player = function($player){
        this.player = $player;
    }

    Checker.prototype.set_type = function(type){
        if(type == 1)
        {
            this.viewObj.find('.crown').css('display','block');
        }else{
            this.viewObj.find('.crown').css('display','none');
        }
        this.type = type;
    }



    return Checker;
});