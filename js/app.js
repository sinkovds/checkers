function preventSelection(element){
    var preventSelection = false;

    function addHandler(element, event, handler){
        if (element.attachEvent)
            element.attachEvent('on' + event, handler);
        else
        if (element.addEventListener)
            element.addEventListener(event, handler, false);
    }
    function removeSelection(){
        if (window.getSelection) { window.getSelection().removeAllRanges(); }
        else if (document.selection && document.selection.clear)
            document.selection.clear();
    }
    function killCtrlA(event){
        var event = event || window.event;
        var sender = event.target || event.srcElement;

        if (sender.tagName.match(/INPUT|TEXTAREA/i))
            return;

        var key = event.keyCode || event.which;
        if (event.ctrlKey && key == 'A'.charCodeAt(0))  // 'A'.charCodeAt(0) можно заменить на 65
        {
            removeSelection();

            if (event.preventDefault)
                event.preventDefault();
            else
                event.returnValue = false;
        }
    }

    // не даем выделять текст мышкой
    addHandler(element, 'mousemove', function(){
        if(preventSelection)
            removeSelection();
    });
    addHandler(element, 'mousedown', function(event){
        var event = event || window.event;
        var sender = event.target || event.srcElement;
        preventSelection = !sender.tagName.match(/INPUT|TEXTAREA/i);
    });

    // борем dblclick
    // если вешать функцию не на событие dblclick, можно избежать
    // временное выделение текста в некоторых браузерах
    addHandler(element, 'mouseup', function(){
        if (preventSelection)
            removeSelection();
        preventSelection = false;
    });

    // борем ctrl+A
    // скорей всего это и не надо, к тому же есть подозрение
    // что в случае все же такой необходимости функцию нужно
    // вешать один раз и на document, а не на элемент
    addHandler(element, 'keydown', killCtrlA);
    addHandler(element, 'keyup', killCtrlA);
}


var GameApp = angular.module('GameApp',['LocalStorageModule']);

GameApp.controller('GameController', ['$scope', 'localStorageService', 'Checker', 'Game', 'Desk', 'Player' ,function GameController($scope,localStorageService, Checker, Game, Desk, Player) {


    var game = new Game();

    game.scope = $scope;

    $scope.game = game;

    $scope.sub_name1 = 'Новый игрок 1';
    $scope.sub_name2 = 'Новый игрок 2';
    $scope.show_new_window = true;
    $scope.win_message = false;

    game.draw();

    var total_score = localStorageService.cookie.get('total_score');

    if(total_score == null)
        $scope.show_new_window = true;
    else {
        $scope.show_new_window = false;
        game.globalscore = total_score;
        game.run();
    }

    var saved_player_1 = localStorageService.cookie.get('player_1');
    var saved_player_2 = localStorageService.cookie.get('player_2');

    if(saved_player_1 != null)
    {
        game.players[0].name = saved_player_1;
        $scope.sub_name1 = saved_player_1;
    }

    if(saved_player_2 != null)
    {
        game.players[1].name = saved_player_2;
        $scope.sub_name2 = saved_player_2;
    }


    $scope.letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");


    $scope.toHHMMSS = function ($seconds) {
        var sec_num = parseInt($seconds, 10); // don't forget the second param
        var hours   = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (hours   < 10) {hours   = "0"+hours;}
        if (minutes < 10) {minutes = "0"+minutes;}
        if (seconds < 10) {seconds = "0"+seconds;}

        var time    = minutes+':'+seconds;
        return time;
    }


    $scope.add_score = function(player_win){
      game.globalscore[player_win]++;

        localStorageService.cookie.set('total_score', game.globalscore);
        localStorageService.cookie.set('player_1', game.players[0].name);
        localStorageService.cookie.set('player_2', game.players[1].name);
        $scope.updateInfo();
    };


    $scope.updateInfo = function(){
        $scope.$apply();
    }

    $scope.run = function(){
        this.reload_game();

        $scope.show_new_window = false;
        game.globalscore = [0,0];
        game.players[0].name = $scope.sub_name1;
        game.players[1].name = $scope.sub_name2;
        localStorageService.cookie.set('total_score', game.globalscore);
        localStorageService.cookie.set('player_1', game.players[0].name);
        localStorageService.cookie.set('player_2', game.players[1].name);

    }

    $scope.reload_game = function(){

        $scope.win_message = false;
        game.remove();
        game.draw();

        var total_score = localStorageService.cookie.get('total_score');
        if(total_score != null){
            game.globalscore = total_score;
        }

        var saved_player_1 = localStorageService.cookie.get('player_1');
        var saved_player_2 = localStorageService.cookie.get('player_2');

        if(saved_player_1 != null)
        {
            game.players[0].name = saved_player_1;
        }

        if(saved_player_2 != null)
        {
            game.players[1].name = saved_player_2;
        }

        game.run();


    }

    $scope.change_players = function(){
        $scope.show_new_window = true;

    }

    $scope.hide_window = function(){
        $scope.show_new_window = false;
    }

    angular.element(document).ready(function () {

        preventSelection(document);

        $(document).on('click','.desk_cell',function(){
            var x = parseInt($(this).attr('x'));
            var y = parseInt($(this).attr('y'));

            var scope = angular.element('body').scope();

            scope.game.set_location(x,y);
            scope.updateInfo();
        });
    });





}]);


